Hello

## Qu’est-ce que le Printlab de l’erg?
Le Printlab est un espace d’expérimentation et d’appropriation de différents outils d’impression et de dessin.
Il a été mis en place pendant l’année scolaire 2016-2017 et se trouve dans la partie annexe du local 1P06.
Actuellement il est accessible à quelques étudiant·e·s ayant suivi une formation l’an dernier et en lien avec quelques cours du pôle Média.
Équipé pour l’instant d’un duplicopieur riso, d’une découpeuse vinyl et de deux plotters, le Printlab vise à mettre en pratique des chemins alternatifs d’impression, la prise en main d’un ensemble de processus de reproduction particuliers.

## Comment fonctionne ce Printlab?
Il s’autofinance. La production de chaque projet est financée au prix minimum des matières premières par les porteurs de chaque projet.
Un devis est calculé à chaque début de projet et ajusté au besoin. Une caisse suivie permet le renouvellement du papier et des consommables.
Plusieurs professeur·e·s se sont investi·e·s dans sa création et suivent son développement: Alexia de Visscher, Ludivine Loiseau, Nicolas Storck, Wendy Van Wynsberghe et Stéphanie Vilayphiou.
Pour les infos, la gestion logistique et technique, Nicolas est l’enseignant référant.

## Et ses Répliquant·e·s?
L’idée est de constituer une petite équipe de pilotage, un groupe d’étudiant·e·s et d’enseignant·e·s qui prennent soin de l’atelier, s’organisent en relais pour le suivi et la documentation des projets.
Cette structure peut être variable et renouvelée chaque année.
Elle est constituée d’une équipe de maximum 10 étudiant·e·s/enseignant·e·s. Avec, dans l’idéal, au minimum 1 étudiant de chaque pôle, 1 étudiant du CE et 1 professeur de chaque pôle.
Leur rôle primordial étant de transmettre leurs connaissances et de superviser la production, ils sont aussi prioritaires pour réaliser leurs projets .
Les Répliquant·e·s suivront une initiation technique afin de connaître les machines et disposeront ensuite de l’accès au local, en devenant les responsables au premier chef.
Pour l'année 2017-2018, les répliquants sont Joseph Decroix, Quentin Duroux, Enzo Garnero, Sebastien Heim, Adrien de Hemptinne, Sarah Humblot, Isabelle Jossa, Bernadette Kluyskens, Ludi Loiseau, Marine Retali, Clara Sambot, Nicolas Storck, Alexia de Visscher.

## Agenda 
C'est ici qu'on va planifier les projets:
https://framagenda.org/index.php/apps/calendar/p/AL1PKF4N5YJXLUQ6/Printlab
