# Modes d'emploi

## Roland DXY1100
### Préparer un dessin en hpgl
* Ouvrir les fichier svg avec Inkscape.
* Sauvegarder au format hpg en décochant l'option “mirror Y-axis”

### Se connecter au plotter
* Ouvrir un terminal
* Taper “sudo chmod 777 /dev/ttyUSB0”
* Valider avec la touche “Entrée”
* Compléter le mot de passe (il reste en blind mode c'est normal)
* Valider avec la touche “Entrée”

### Lancer le dessin 
* Depuis le terminal, déplacer vous vers le dossier de travail avec la commande suivant :
$ cd Documents/hpgl
* Entrer la commande suivante pour envoyer le dessin au plotter :
$ plot_hpgl_file.py mondessin.hpgl
* Sélection l'option 2 ou 11 et valider avec la touche “Entrée”
